# -*- coding: utf-8 -*-
# Copyright (c) 2015 www.jzyx.com

import json, os, sys, subprocess, getopt
import cmd

top_project = ""
sub_project = ""

repo_count = 0
repos = []

# json file name, get from command line
json_file = ""

# json file path
json_path = ""
json_data = {}
json_path=""

dirname = os.path.dirname

# project/piscripts/tools/*.py
project_root = dirname(dirname(dirname(os.path.abspath(__file__))))
scripts_root = os.path.join(project_root, "piscripts")

save_path = ""

# this scipts tool dir name
# piscripts/tool/*.py -> piscripts
tool_script_root = dirname(dirname(os.path.abspath(__file__)))
tool_script_dir = os.path.basename(dirname(dirname(os.path.abspath(__file__))))

# project/piscripts/json
json_root = os.path.join(tool_script_root, "json")

def read_json_from_file():
    global json_data, json_file, json_path, scripts_root
    json_dir = os.path.join(scripts_root, "json")
    json_path = os.path.join(json_dir, json_file)

    with open(json_path) as datafile:
        json_data = json.load(datafile)

def parseJSONData():
    global json_data, repos, top_project, sub_project
    top_project = json_data['project']['top']
    sub_project = json_data['project']['sub']
    repos = json_data['repos']

def addGitAppPath():
    exe='git.exe'
    cmd='where {0}'.format(exe)
    exe_path =subprocess.check_output(cmd)
    exe_dir =os.path.dirname(exe_path)
    path=os.environ['PATH']
    os.environ['PATH'] += ";" + exe_dir

def enterProjectRoot():
    global project_root, save_path
    save_path = os.getcwd()
    os.chdir(project_root)

def exitProjectRoot():
    global save_path
    os.chdir(save_path)

def addRepoInfoToCmd(gitcmd, repo): 
    gitcmd.add_cmd("echo ''")
    gitcmd.add_cmd("color='\e[0;32m'; purple='\e[0;33m'; echo -e \"${color}------------------------------------------------------------${purple}\"")
    gitcmd.add_cmd("echo project: {0}".format(repo))
    gitcmd.add_cmd("white='\e[0;37m'; echo -e \"${white}\"")    

# clone git repos
def git_clone(gitcmd, info): 
    gitcmd.add_git_cmd("clone", info['local'], info['remote'])

# pull git repos
def git_pull(gitcmd, info): 
    gitcmd.add_git_cmd("pull", info['local'])

def show_repo(gitcmd, repo, info): 
    gitcmd.add_cmd("echo json url ------")
    gitcmd.add_cmd("echo origin: {0}".format(info['remote']))
    gitcmd.add_cmd("echo real url ------")
    gitcmd.add_git_cmd('remote -v', repo, "", "")
    gitcmd.add_git_cmd('status -v', repo, "", "")

# clone && pull && checkout
def git_udpate():
    global project_root
    read_json_from_file()
    parseJSONData()
    addGitAppPath()
    
    enterProjectRoot()
    gitcmd = cmd.GitCmd()
    for repo, info in repos.items():
        addRepoInfoToCmd(gitcmd, repo)
        if not os.path.exists(info['local']):
            #gitcmd.add_git_cmd("clone", info['local'], info['remote'], "")
            git_clone(gitcmd, info)
        else:
            #gitcmd.add_git_cmd("pull", info['local'], info['remote'], "")
            git_pull(gitcmd, info)

        gitcmd.add_git_cmd('checkout', info['local'], info['remote'], info['branch'])
        show_repo(gitcmd, repo, info)

    gitcmd.invoke()
    exitProjectRoot()

def git_status():
    # should show status of all.
    global project_root, tool_script_dir
    
    enterProjectRoot()
    gitcmd = cmd.GitCmd()

    for root, dirs, files in os.walk(project_root):
        if root == project_root:
            for repo in dirs:
                if repo != tool_script_dir:
                    addRepoInfoToCmd(gitcmd, repo)
                    gitcmd.add_git_cmd("remote -v", repo, "", "")
                    gitcmd.add_git_cmd("status -v", repo, "", "")
            break;
    gitcmd.invoke()
    exitProjectRoot()

def main(argv):
    try:
        opts, args =getopt.getopt(argv, "f:s", ["file="])
    except:
        print "args error!"
        sys.exit(2)

    global json_file
    for opt, arg in opts:
        if opt in ("-f", "--file"):
            json_file=arg
            git_udpate()
        if opt == "-s":
            git_status()

if __name__ == "__main__":
    main(sys.argv[1:])
