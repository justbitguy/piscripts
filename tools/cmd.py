# -*- codding:utf-8 -*- 

import os 

# Cause git cmd need to execute in sh.exe, 
# to avoid logining every time when executing,
# here add new command in pending status
# with ";" connector, and invoking them at last.
class ShCmd:
	def __init__(self): 
		self.cmd = "sh.exe --login -c \""
		self.cmd_count = 0

	def print_cmd(self): 
		print "%s" %self.cmd

	def add_cmd(self, cmd):
		if cmd == "":
			return 

		if self.cmd_count == 0:
			self.cmd += cmd
		else:
			self.cmd += " ; " + cmd

		self.cmd_count += 1

	def invoke(self): 
		self.cmd += "\""
		os.system(self.cmd)
		self.cmd=""


class GitCmd(ShCmd): 
	def __init__(self): 
		ShCmd.__init__(self)

	def add_git_cmd(self, op, local="", remote="", branch=""):
		if op == "clone":
		    cmd = "git clone {0} {1}".format(remote, local)
		elif op == "reset": 
			cmd = "cd {0} ; git reset --hard HEAD ; cd .. ".format(local)
		elif op == "clean": 
			cmd = "cd {0} ; git clean -fx -d ; cd .. ".format(local)
		elif op == "pull":
		    cmd = "cd {0} ; git pull ; cd .. ".format(local)
		elif op == "checkout":
		    cmd = "cd {0} ; git checkout {1} ; cd .. ".format(local, branch)
		elif op == "status -v":
		    cmd = "cd {0} ; git {1} ; cd .. ".format(local, op)
		elif op == "remote -v":
		    cmd = "cd {0} ; git {1} ; cd .. ".format(local, op)
		elif op == "set-url": 
			cmd = "cd {0} ; git remote set-url origin {1} ; cd ..".format(local, remote)
		
		ShCmd.add_cmd(self, cmd)