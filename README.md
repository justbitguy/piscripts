# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* 在引擎和项目开发中，存在一个项目对应多git库的情况，这种项目一般以 项目名 + 分支名 来简单划分;
* 此工具用于clone或者pull项目对应的git库，也用于查看当前项目的git状态；  
* Version 0.5.

### 更新项目的git库 ###
* 创建项目目录，命名没有规定
* 在json目录下，创建项目对应的json文件，命名格式为: project-branch.json(如p23-int.json, engine-master.json)；
* json文件的格式可以参考p23-int.json, 其中top对应project, branch对应sub;
* 创建对应.bat文件，格式如：
      python tools\git_tool.py -f project-branch.json

* 双击.bat文件，更新项目库

### 查看git库的状态  ###
* 双击git-status.bat: 文件脚本会查看project项目下的所有git项目，不管是否在json文件中指定了配置  

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact